# merlin

#### 项目介绍
- 项目名称: merlin
- 所属系列：openharmony的第三方组件适配移植
- 功能：手机，wifi网络状态监听
- 项目移植状态：完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 1.2.1

#### 安装教程
1.在项目根目录下的build.gradle文件中，
```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```
2.在entry模块的build.gradle文件中，
```gradle
merlin
dependencies {
   implementation('com.gitee.chinasoft_ohos:merlin:1.0.0')
   ......  
}
merlin_ rxjava
dependencies {
   implementation('com.gitee.chinasoft_ohos:merlin_rxjava:1.0.0')
   ......  
}
merlin_rxjava2
dependencies {
   implementation('com.gitee.chinasoft_ohos:merlin_rxjava2:1.0.0')
   ......  
}
```
在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下
#### 效果演示

![输入图片说明](https://gitee.com/chinasoft_ohos/merlin/raw/MER-163/null_pointer/gif/demo.gif  "demo.gif")

#### 使用说明

1. 所需权限:
	```config.json
	"reqPermissions": [
		{
		"name": "ohos.permission.GET_NETWORK_INFO"
		},
		{
		"name": "ohos.permission.INTERNET"
		},
		{
		"name": "ohos.permission.GET_WIFI_INFO"
		}
	```

2. 创建Merlin对象:
 
	```java
	merlin = new Merlin.Builder().withConnectableCallbacks().build(context);
	```

3. 绑定到AbilitySlice:

	```java
	@Override
	public void onStart(Intent intent) {
		super.onStart(intent);
		merlin.bind();
	}
	@Override
	protected void onStop() {
		super.onStop();
		merlin.unbind();
	}
	```

4. 注册监听
	```java
	merlin.registerConnectable(new Connectable() {
		@Override
		public void onConnect() {
			// Do something you haz internet!
		}
	});
	merlin.registerDisconnectable(new Disconnectable() {
		@Override
		public void onDisconnect() {
			// Do something you haz internet!
		}
	});
	merlin.registerBindable(new Bindable() {
		@Override
		public void onBind() {
			// Do something you haz internet!
		}
	});
	```
	
 5. 扩展
	```groovy
	// For RxJava 1.x
	implementation 'com.novoda:merlin-rxjava:[version_number]'

	// For RxJava 2.x
	implementation 'com.novoda:merlin-rxjava2:[version_number]'
	```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代
- 1.0.0

#### 版权和许可信息
Apache 2.0
