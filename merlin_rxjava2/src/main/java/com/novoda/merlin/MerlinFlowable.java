package com.novoda.merlin;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import ohos.app.Context;

/**
 * MerlinFlowable
 *
 * @since 2021-04-13
 */
public class MerlinFlowable {
    /**
     * from
     *
     * @param context
     * @return Flowable
     */
    public static Flowable<NetworkStatus> from(Context context) {
        return from(context, new Merlin.Builder());
    }

    /**
     * from
     *
     * @param context
     * @param merlinBuilder
     * @return Flowable
     */
    public static Flowable<NetworkStatus> from(Context context, MerlinBuilder merlinBuilder) {
        return from(merlinBuilder.withAllCallbacks()
                            .build(context));
    }

    /**
     * from
     *
     * @param merlin
     * @return Flowable
     */
    public static Flowable<NetworkStatus> from(Merlin merlin) {
        return createFlowable(merlin);
    }

    private static Flowable<NetworkStatus> createFlowable(Merlin merlin) {
        return Flowable.create(new MerlinFlowableOnSubscribe(merlin), BackpressureStrategy.LATEST);
    }
}
