package com.novoda.merlin;

/**
 * Request
 *
 * @since 2021-04-13
 */
interface Request {
    int getResponseCode();
}
