package com.novoda.merlin;

/**
 * 未连接回调管理
 *
 * @since 2021-04-13
 */
class DisconnectCallbackManager extends MerlinCallbackManager<Disconnectable> implements Disconnectable {
    DisconnectCallbackManager(Register<Disconnectable> register) {
        super(register);
    }

    @Override
    public void onDisconnect() {
        Logger.d("onDisconnect");
        for (Disconnectable disconnectable : registerables()) {
            disconnectable.onDisconnect();
        }
    }
}
