package com.novoda.merlin;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;

/**
 * PingTask
 *
 * @since 2021-04-13
 */
class PingTask implements Runnable {
    private final Ping ping;
    private final EndpointPinger.PingerCallback pingerCallback;
    private OnCompleteHandler onCompleteHandler;

    PingTask(Ping ping, EndpointPinger.PingerCallback pingerCallback) {
        this.ping = ping;
        this.pingerCallback = pingerCallback;
        EventRunner eventRunner = EventRunner.getMainEventRunner();
        setOnCompleteHandler(new OnCompleteHandler(eventRunner));
    }

    /**
     * OnCompleteHandler
     *
     * @since 2021-04-13
     */
    public class OnCompleteHandler extends EventHandler {
        OnCompleteHandler(EventRunner runner) throws IllegalArgumentException {
            super(runner);
        }

        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            onComplete((boolean)event.object);
        }

        private void onComplete(boolean isPingResultIsSuccess) {
            if (isPingResultIsSuccess) {
                pingerCallback.onSuccess();
            } else {
                pingerCallback.onFailure();
            }
        }
    }

    @Override
    public void run() {
        complete(ping.doSynchronousPing());
    }

    private void setOnCompleteHandler(OnCompleteHandler handler) {
        this.onCompleteHandler = handler;
    }

    private void complete(boolean isPingResultIsSuccess) {
        if (onCompleteHandler != null) {
            int eventId = 0;
            long param = 0L;
            InnerEvent innerEvent = InnerEvent.get(eventId, param, isPingResultIsSuccess);
            onCompleteHandler.sendEvent(innerEvent);
        }
    }
}
