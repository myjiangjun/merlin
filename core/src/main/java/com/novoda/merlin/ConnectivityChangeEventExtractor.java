package com.novoda.merlin;

import ohos.app.Context;
import ohos.net.NetHandle;

/**
 * 网络状态变化处理执行
 *
 * @since 2021-04-13
 */
class ConnectivityChangeEventExtractor {
    private final Context mContext;

    ConnectivityChangeEventExtractor(Context context) {
        mContext = context;
    }

    ConnectivityChangeEvent extractFrom(NetHandle netHandle) {
        NetworkInfo networkInfo = NetworkInfo.getInstance(mContext);

        if (netHandle != null) {
            boolean isConnected = networkInfo.isConnected();
            String reason = "";
            String extraInfo = "";
            return ConnectivityChangeEvent.createWithNetworkInfoChangeEvent(isConnected, extraInfo, reason);
        } else {
            return ConnectivityChangeEvent.createWithoutConnection();
        }
    }
}
