package com.novoda.merlin;

import java.io.IOException;

/**
 * RequestException
 *
 * @since 2021-04-13
 */
class RequestException extends RuntimeException {
    RequestException(Throwable e) {
        super(e);
    }

    boolean causedByIo() {
        return getCause() instanceof IOException;
    }
}
