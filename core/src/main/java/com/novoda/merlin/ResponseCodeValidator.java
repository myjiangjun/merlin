package com.novoda.merlin;

/**
 * ResponseCodeValidator
 *
 * @since 2021-04-13
 */
public interface ResponseCodeValidator {
    /**
     * RESPONSE_CODE
     */
    int RESPONSE_CODE = 200;

    /**
     * isResponseCodeValid
     *
     * @param responseCode
     * @return boolean
     */
    boolean isResponseCodeValid(int responseCode);

    /**
     * CaptivePortalResponseCodeValidator
     *
     * @since 2021-04-13
     */
    class CaptivePortalResponseCodeValidator implements ResponseCodeValidator {
        @Override
        public boolean isResponseCodeValid(int responseCode) {
            return responseCode == RESPONSE_CODE;
        }
    }
}
