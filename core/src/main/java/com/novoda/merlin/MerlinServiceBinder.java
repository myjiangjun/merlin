package com.novoda.merlin;

import ohos.aafwk.ability.IAbilityConnection;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.app.Context;
import ohos.bundle.ElementName;
import ohos.net.NetManager;
import ohos.rpc.IRemoteObject;

/**
 * MerlinServiceBinder
 *
 * @since 2021-04-13
 */
class MerlinServiceBinder {
    private final Context context;
    private final ListenerHolder listenerHolder;

    private ResponseCodeValidator validator;
    private MerlinServiceConnection merlinServiceConnection;
    private Endpoint endpoint;

    MerlinServiceBinder(Context context, ConnectCallbackManager connectCallbackManager,
                        DisconnectCallbackManager disconnectCallbackManager,
                        BindCallbackManager bindCallbackManager, Endpoint endpoint, ResponseCodeValidator validator) {
        this.validator = validator;
        listenerHolder = new ListenerHolder(connectCallbackManager, disconnectCallbackManager, bindCallbackManager);
        this.context = context;
        this.endpoint = endpoint;
    }

    void setEndpoint(Endpoint endPoint, ResponseCodeValidator responseCodeValidator) {
        this.endpoint = endPoint;
        this.validator = responseCodeValidator;
    }

    void bindService() {
        if (merlinServiceConnection == null) {
            merlinServiceConnection = new MerlinServiceConnection(context, listenerHolder, endpoint, validator);
        }
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName("com.novoda.merlin.demo")
                .withAbilityName("com.novoda.merlin.MerlinService")
                .build();
        intent.setOperation(operation);
        context.connectAbility(intent, merlinServiceConnection);
    }

    void unbind() {
        if (GlobalData.getInstance().isBound() && merlinServiceConnection != null) {
            context.disconnectAbility(merlinServiceConnection);
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("com.novoda.merlin.demo")
                    .withAbilityName("com.novoda.merlin.MerlinService")
                    .build();
            intent.setOperation(operation);
            context.stopAbility(intent);
            merlinServiceConnection = null;
        }
    }

    /**
     * MerlinServiceConnection
     *
     * @since 2021-04-13
     */
    private static class MerlinServiceConnection implements IAbilityConnection {
        private final Context context;
        private final ListenerHolder listenerHolder;
        private final Endpoint endpoint;
        private final ResponseCodeValidator validator;

        MerlinServiceConnection(Context context,
                                ListenerHolder listenerHolder, Endpoint endpoint, ResponseCodeValidator validator) {
            this.context = context;
            this.listenerHolder = listenerHolder;
            this.endpoint = endpoint;
            this.validator = validator;
        }

        @Override
        public void onAbilityConnectDone(ElementName elementName, IRemoteObject interfaceRemoteObject, int resultCode) {
            Logger.d("onServiceConnected");
            MerlinService.LocalBinder merlinServiceBinder = (MerlinService.LocalBinder) interfaceRemoteObject;
            NetManager connectivityManager = NetManager.getInstance(context);
            ConnectivityChangeEventExtractor connectivityChangeEventExtractor =
                    new ConnectivityChangeEventExtractor(context);
            ConnectivityChangesRegister connectivityChangesRegister = new ConnectivityChangesRegister(
                    connectivityManager,
                    connectivityChangeEventExtractor
            );
            NetworkStatusRetriever networkStatusRetriever = new NetworkStatusRetriever(MerlinsBeard.from(context));
            EndpointPinger endpointPinger = EndpointPinger.withCustomEndpointAndValidation(endpoint, validator);
            ConnectivityChangesForwarder connectivityChangesForwarder = new ConnectivityChangesForwarder(
                    networkStatusRetriever,
                    listenerHolder.disconnectCallbackManager,
                    listenerHolder.connectCallbackManager,
                    listenerHolder.bindCallbackManager,
                    endpointPinger
            );

            merlinServiceBinder.setConnectivityChangesRegister(connectivityChangesRegister);
            merlinServiceBinder.setConnectivityChangesForwarder(connectivityChangesForwarder);
            merlinServiceBinder.onBindComplete();
        }

        @Override
        public void onAbilityDisconnectDone(ElementName elementName, int resultCode) {
            // do nothing.
        }
    }

    /**
     * ListenerHolder
     *
     * @since 2021-04-13
     */
    private static class ListenerHolder {
        private final DisconnectCallbackManager disconnectCallbackManager;
        private final ConnectCallbackManager connectCallbackManager;
        private final BindCallbackManager bindCallbackManager;

        ListenerHolder(ConnectCallbackManager connectCallbackManager,
                       DisconnectCallbackManager disconnectCallbackManager, BindCallbackManager bindCallbackManager) {
            this.connectCallbackManager = connectCallbackManager;
            this.disconnectCallbackManager = disconnectCallbackManager;
            this.bindCallbackManager = bindCallbackManager;
        }
    }
}
