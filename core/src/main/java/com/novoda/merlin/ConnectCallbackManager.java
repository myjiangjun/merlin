package com.novoda.merlin;

/**
 * 网络监听服务已连接回调
 *
 * @since 2021-04-13
 */
class ConnectCallbackManager extends MerlinCallbackManager<Connectable> {
    ConnectCallbackManager(Register<Connectable> register) {
        super(register);
    }

    void onConnect() {
        Logger.d("onConnect");
        for (Connectable connectable : registerables()) {
            connectable.onConnect();
        }
    }
}
