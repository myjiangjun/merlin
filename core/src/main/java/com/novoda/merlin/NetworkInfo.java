/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.novoda.merlin;

import ohos.app.Context;
import ohos.net.NetManager;
import ohos.telephony.CellularDataInfoManager;
import ohos.telephony.RadioInfoManager;
import ohos.telephony.SignalInformation;
import ohos.telephony.TelephonyConstants;
import ohos.wifi.WifiDevice;

import java.util.List;

/**
 * NetworkInfo
 *
 * @since 2021-04-13
 */
public class NetworkInfo {
    /**
     * TYPE_MOBILE
     */
    public static final int TYPE_MOBILE = 0;
    /**
     * TYPE_WIFI
     */
    public static final int TYPE_WIFI = 1;
    private static NetworkInfo networkInfo;
    private final Context mContext;
    private final RadioInfoManager radioInfoManager;
    private final WifiDevice wifiDevice;
    private final CellularDataInfoManager cellularDataInfoManager;

    private NetworkInfo(Context context) {
        mContext = context;
        radioInfoManager = RadioInfoManager.getInstance(mContext);
        wifiDevice = WifiDevice.getInstance(mContext);
        cellularDataInfoManager = CellularDataInfoManager.getInstance(mContext);
    }

    /**
     * NetworkInfo getInstance
     *
     * @param context
     * @return NetworkInfo
     */
    public static NetworkInfo getInstance(Context context) {
        if (networkInfo == null) {
            networkInfo = new NetworkInfo(context);
        }
        return networkInfo;
    }

    public boolean isConnected() {
        return NetManager.getInstance(mContext).getAllNets() != null
                && NetManager.getInstance(mContext).getAllNets().length > 0;
    }

    /**
     * getNetworkInfo
     *
     * @param networkType
     * @return boolean
     */
    public boolean getNetworkInfo(int networkType) {
        if (networkType == TYPE_MOBILE) {
            return cellularDataInfoManager.isCellularDataEnabled()
                    && cellularDataInfoManager.getCellularDataState(
                            cellularDataInfoManager.getDefaultCellularDataSlotId())
                    == CellularDataInfoManager.DATA_STATE_CONNECTED;
        } else {
            return wifiDevice.isConnected();
        }
    }

    /**
     * getSubtypeName
     *
     * @return String
     */
    public String getSubtypeName() {
        // 获取信号信息。
        List<SignalInformation> signalList = radioInfoManager.getSignalInfoList(radioInfoManager.getPrimarySlotId());
        if (signalList.size() == 0) {
            return "";
        }
        String subtypeName = "";
        switch (signalList.get(0).getNetworkType()) {
            case TelephonyConstants.NETWORK_TYPE_GSM:
                subtypeName = "GSM";
                break;
            case TelephonyConstants.NETWORK_TYPE_CDMA:
                subtypeName = "CDMA";
                break;
            case TelephonyConstants.NETWORK_TYPE_WCDMA:
                subtypeName = "WCDMA";
                break;
            case TelephonyConstants.NETWORK_TYPE_TDSCDMA:
                subtypeName = "TDSCDMA";
                break;
            case TelephonyConstants.NETWORK_TYPE_LTE:
                subtypeName = "LTE";
                break;
            case TelephonyConstants.NETWORK_TYPE_NR:
                subtypeName = "NR";
                break;
            default:
                break;
        }
        return subtypeName;
    }
}
