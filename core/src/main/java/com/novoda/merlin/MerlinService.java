package com.novoda.merlin;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.LocalRemoteObject;
import ohos.aafwk.content.Intent;
import ohos.rpc.IRemoteObject;

/**
 * MerlinService
 *
 * @since 2021-04-13
 */
public class MerlinService extends Ability {
    private static boolean isBound;
    private IRemoteObject binder = new LocalBinder();
    private ConnectivityChangesRegister connectivityChangesRegister;
    private ConnectivityChangesForwarder connectivityChangesForwarder;

    @Override
    public IRemoteObject onConnect(Intent intent) {
        GlobalData.getInstance().setBound(true);
        return binder;
    }

    @Override
    public void onDisconnect(Intent intent) {
        GlobalData.getInstance().setBound(false);
        if (connectivityChangesRegister != null) {
            connectivityChangesRegister.unregister();
            connectivityChangesRegister = null;
        }

        if (connectivityChangesForwarder != null) {
            connectivityChangesForwarder = null;
        }

        binder = null;
        super.onDisconnect(intent);
    }

    private void start() {
        assertDependenciesBound();
        connectivityChangesForwarder.forwardInitialNetworkStatus();
        connectivityChangesRegister.register((ConnectivityChangesNotifier) binder);
    }

    private void assertDependenciesBound() {
        if (MerlinService.this.connectivityChangesRegister == null) {
            throw MerlinServiceDependencyMissingException.missing(ConnectivityChangesRegister.class);
        }

        if (MerlinService.this.connectivityChangesForwarder == null) {
            throw MerlinServiceDependencyMissingException.missing(ConnectivityChangesForwarder.class);
        }
    }

    /**
     * ConnectivityChangesNotifier
     *
     * @since 2021-04-13
     */
    public interface ConnectivityChangesNotifier {
        /**
         * canNotify
         *
         * @return boolean
         */
        boolean canNotify();

        /**
         * MerlinsBeardBuilder
         *
         * @param connectivityChangeEvent
         */
        void notify(ConnectivityChangeEvent connectivityChangeEvent);
    }

    /**
     * LocalBinder
     *
     * @since 2021-04-13
     */
    class LocalBinder extends LocalRemoteObject implements ConnectivityChangesNotifier {
        LocalBinder() {
            super();
        }

        @Override
        public boolean canNotify() {
            return GlobalData.getInstance().isBound();
        }

        @Override
        public void notify(ConnectivityChangeEvent connectivityChangeEvent) {
            // Workaround for https://github.com/novoda/merlin/issues/163 & 171, further work required.
            if (canNotify() && MerlinService.this.connectivityChangesForwarder != null) {
                MerlinService.this.connectivityChangesForwarder.forward(connectivityChangeEvent);
            } else {
                Logger.w("notify event dropped due to inconsistent service state");
            }
        }

        void setConnectivityChangesRegister(ConnectivityChangesRegister connectivityChangesRegister) {
            MerlinService.this.connectivityChangesRegister = connectivityChangesRegister;
        }

        void setConnectivityChangesForwarder(ConnectivityChangesForwarder connectivityChangesForwarder) {
            MerlinService.this.connectivityChangesForwarder = connectivityChangesForwarder;
        }

        void onBindComplete() {
            MerlinService.this.start();
        }
    }
}
