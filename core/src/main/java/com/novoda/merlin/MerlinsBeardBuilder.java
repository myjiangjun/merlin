package com.novoda.merlin;

import ohos.app.Context;

/**
 * MerlinsBeardBuilder
 *
 * @since 2021-04-13
 */
public class MerlinsBeardBuilder {
    private Endpoint endpoint = Endpoint.captivePortalEndpoint();
    private ResponseCodeValidator responseCodeValidator =
            new ResponseCodeValidator.CaptivePortalResponseCodeValidator();

    MerlinsBeardBuilder() {
        // Uses builder pattern.
    }

    /**
     * Sets a custom endPoint.
     *
     * @param endPoint to ping, by default .
     * @return MerlinsBeardBuilder.
     */
    public MerlinsBeardBuilder withEndpoint(Endpoint endPoint) {
        this.endpoint = endPoint;
        return this;
    }

    /**
     * Validator used to check the response code when performing a ping.
     *
     * @param validator
     * A validator implementation used for checking that the response code is what you expect.
     * The default endpoint returns a 204 No Content response, so the default validator checks for that.
     *
     * @return MerlinsBeardBuilder.
     */
    public MerlinsBeardBuilder withResponseCodeValidator(ResponseCodeValidator validator) {
        this.responseCodeValidator = validator;
        return this;
    }

    /**
     * MerlinsBeard build
     *
     * @param context
     * @return MerlinsBeard
     */
    public MerlinsBeard build(Context context) {
        EndpointPinger captivePortalpinger =
                EndpointPinger.withCustomEndpointAndValidation(endpoint, responseCodeValidator);
        Ping captivePortalPing = new Ping(endpoint, new EndpointPinger.ResponseCodeFetcher(), responseCodeValidator);
        NetworkInfo networkInfo = NetworkInfo.getInstance(context);
        return new MerlinsBeard(captivePortalpinger, captivePortalPing,networkInfo);
    }
}
