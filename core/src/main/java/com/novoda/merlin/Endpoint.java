package com.novoda.merlin;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Objects;

/**
 * 网络状态接口对象
 *
 * @since 2021-04-13
 */
public class Endpoint {
    private static final Endpoint CAPTIVE_PORTAL_ENDPOINT =
            Endpoint.from("https://connectivitycheck.ohos.com/generate_204");
    private final String endpoint;

    private Endpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    /**
     * 网络状态接口对象
     *
     * @return Endpoint
     */
    public static Endpoint captivePortalEndpoint() {
        return CAPTIVE_PORTAL_ENDPOINT;
    }

    /**
     * 初始化网络状态接口对象
     *
     * @param endpoint
     * @return Endpoint
     */
    public static Endpoint from(String endpoint) {
        return new Endpoint(endpoint);
    }

    /**
     * 获取网络接口URl
     *
     * @return URL
     * @throws MalformedURLException
     */
    public URL asUrl() throws MalformedURLException {
        return new URL(endpoint);
    }

    @Override
    public String toString() {
        return "Endpoint{"
                + "endpoint='"
                + endpoint
                + '\''
                + '}';
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        Endpoint endpoint1 = (Endpoint) object;
        return Objects.equals(endpoint, endpoint1.endpoint);
    }

    @Override
    public int hashCode() {
        return endpoint != null ? endpoint.hashCode() : 0;
    }
}
