package com.novoda.merlin;


import ohos.app.Context;
import ohos.net.NetManager;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

public class ConnectivityChangesRegisterTest {

    private final Context context = spy(Context.class);
    private final MerlinService.ConnectivityChangesNotifier connectivityChangesNotifier = mock(MerlinService.ConnectivityChangesNotifier.class);
    private final ConnectivityChangeEventExtractor extractor = mock(ConnectivityChangeEventExtractor.class);
    private ConnectivityChangesRegister connectivityChangesRegister = mock(ConnectivityChangesRegister.class);;

    @Before
    public void setUp() {
    }

    @Test
    public void givenRegisteredMerlinNetworkCallbacks_whenBindingForASecondTime_thenOriginalNetworkCallbacksIsRegisteredAgain() {
        connectivityChangesRegister.register(connectivityChangesNotifier);
    }

    @Test
    public void givenRegisteredMerlinNetworkCallback_whenUnbinding_thenUnregistersOriginallyRegisteredNetworkCallbacks() {
        connectivityChangesRegister.unregister();
    }
}
