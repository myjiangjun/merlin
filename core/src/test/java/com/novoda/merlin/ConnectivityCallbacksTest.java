package com.novoda.merlin;

import ohos.app.Context;
import ohos.net.NetHandle;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class ConnectivityCallbacksTest {

    private static final boolean CONNECTED = true;
    private static final boolean DISCONNECTED = false;

    private static final int MAX_MS_TO_LIVE = 0;

    private static final NetHandle MISSING_NETWORK = null;
    private static final boolean CAN_NOTIFY = true;
    private static final boolean CANNOT_NOTIFY = false;

    private final Context context = spy(Context.class);
    private final MerlinService.ConnectivityChangesNotifier connectivityChangesNotifier = mock(MerlinService.ConnectivityChangesNotifier.class);
    private final NetHandle network = mock(NetHandle.class);

    private ConnectivityCallbacks networkCallbacks  = mock(ConnectivityCallbacks.class);;

    @Before
    public void setUp() {
        given(connectivityChangesNotifier.canNotify()).willReturn(CAN_NOTIFY);
    }

    @Test
    public void givenConnectedNetworkInfo_whenNetworkIsAvailable_thenNotifiesOfConnectedNetwork() {
        NetworkInfo networkInfo = givenNetworkInfoWith(CONNECTED);
        networkCallbacks.onAvailable(network);
    }

    @Test
    public void givenDisconnectedNetworkInfo_whenLosingNetwork_thenNotifiesOfDisconnectedNetwork() {
        NetworkInfo networkInfo = givenNetworkInfoWith(DISCONNECTED);

        networkCallbacks.onLosing(network, MAX_MS_TO_LIVE);
    }

    @Test
    public void givenDisconnectedNetworkInfo_whenNetworkIsLost_thenNotifiesOfLostNetwork() {
        NetworkInfo networkInfo = givenNetworkInfoWith(DISCONNECTED);

        networkCallbacks.onLost(network);

    }

    @Test
    public void givenNoNetworkInfo_whenNetworkIsAvailable_thenNotifiesOfConnectedNetwork() {
        networkCallbacks.onAvailable(network);
    }

    @Test
    public void givenNoNetworkInfo_whenLosingNetwork_thenNotifiesOfDisconnectedNetwork() {
        networkCallbacks.onLosing(network, MAX_MS_TO_LIVE);
    }

    @Test
    public void givenNoNetworkInfo_whenNetworkIsLost_thenNotifiesOfLostNetwork() {
        networkCallbacks.onLost(network);
    }

    @Test
    public void givenCannotNotify_whenNetworkIsAvailable_thenNeverNotifiesConnectivityChangeEvent() {
        given(connectivityChangesNotifier.canNotify()).willReturn(CANNOT_NOTIFY);

        networkCallbacks.onAvailable(MISSING_NETWORK);

        InOrder inOrder = inOrder(connectivityChangesNotifier);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void givenCannotNotify_whenLosingNetwork_thenNeverNotifiesConnectivityChangeEvent() {
        given(connectivityChangesNotifier.canNotify()).willReturn(CANNOT_NOTIFY);

        networkCallbacks.onLosing(MISSING_NETWORK, MAX_MS_TO_LIVE);

        InOrder inOrder = inOrder(connectivityChangesNotifier);
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void givenCannotNotify_whenNetworkIsLost_thenNeverNotifiesConnectivityChangeEvent() {
        given(connectivityChangesNotifier.canNotify()).willReturn(CANNOT_NOTIFY);

        networkCallbacks.onLost(MISSING_NETWORK);

        InOrder inOrder = inOrder(connectivityChangesNotifier);
        inOrder.verifyNoMoreInteractions();
    }

    private NetworkInfo givenNetworkInfoWith(boolean connected) {
        NetworkInfo networkInfo = mock(NetworkInfo.class);
        given(networkInfo.isConnected()).willReturn(connected);
        return networkInfo;
    }

}
