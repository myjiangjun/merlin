package com.novoda.merlin;

import ohos.app.Context;
import ohos.net.NetHandle;
import org.junit.Before;
import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;


public class ConnectivityChangeEventExtractorTest {

    private static final boolean CONNECTED = true;

    private static final String ANY_REASON = "reason";
    private static final String ANY_EXTRA_INFO = "extra info";

    private final Context context = mock(Context.class);
    private static final NetHandle ANY_NETWORK = mock(NetHandle.class);
    private static final NetworkInfo UNAVAILABLE_NETWORK_INFO = null;

    private final NetworkInfo networkInfo = mock(NetworkInfo.class);

    private ConnectivityChangeEventExtractor eventExtractor;

    @Before
    public void setUp() {
        given(networkInfo.isConnected()).willReturn(CONNECTED);


        eventExtractor = new ConnectivityChangeEventExtractor(context);
    }

    @Test
    public void givenNetworkInfo_whenExtracting_thenReturnsConnectivityChangeEvent() {
        given(NetworkInfo.getInstance(context)).willReturn(networkInfo);

        ConnectivityChangeEvent connectivityChangeEvent = eventExtractor.extractFrom(ANY_NETWORK);

        assertThat(connectivityChangeEvent).isEqualTo(ConnectivityChangeEvent.createWithNetworkInfoChangeEvent(
                CONNECTED,
                ANY_EXTRA_INFO,
                ANY_REASON
        ));
    }

    @Test
    public void givenNetworkInfoIsUnavailable_whenExtracting_thenReturnsConnectivityChangeEventWithoutConnection() {
        given(NetworkInfo.getInstance(context)).willReturn(UNAVAILABLE_NETWORK_INFO);

        ConnectivityChangeEvent connectivityChangeEvent = eventExtractor.extractFrom(ANY_NETWORK);

        assertThat(connectivityChangeEvent).isEqualTo(ConnectivityChangeEvent.createWithoutConnection());
    }

}
