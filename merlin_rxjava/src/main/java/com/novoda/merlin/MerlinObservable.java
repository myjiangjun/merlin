package com.novoda.merlin;

import ohos.app.Context;
import rx.Emitter;
import rx.Observable;

/**
 * MerlinObservable
 *
 * @since 2021-04-13
 */
public class MerlinObservable {
    /**
     * from
     *
     * @param context
     * @return Observable
     */
    public static Observable<NetworkStatus> from(Context context) {
        return from(context, new Merlin.Builder());
    }

    /**
     * from
     *
     * @param context
     * @param merlinBuilder
     * @return Observable
     */
    public static Observable<NetworkStatus> from(Context context, MerlinBuilder merlinBuilder) {
        return from(merlinBuilder.withAllCallbacks()
                            .build(context));
    }

    /**
     * from
     *
     * @param merlin
     * @return Observable
     */
    public static Observable<NetworkStatus> from(Merlin merlin) {
        return createObservable(merlin);
    }

    private static Observable<NetworkStatus> createObservable(Merlin merlin) {
        return Observable.create(new MerlinAction(merlin), Emitter.BackpressureMode.LATEST);
    }
}
