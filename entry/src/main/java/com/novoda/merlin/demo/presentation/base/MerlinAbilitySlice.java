package com.novoda.merlin.demo.presentation.base;

import com.novoda.merlin.Bindable;
import com.novoda.merlin.Connectable;
import com.novoda.merlin.Disconnectable;
import com.novoda.merlin.Logger;
import com.novoda.merlin.Merlin;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * MerlinAbilitySlice
 *
 * @since 2021-04-13
 */
public abstract class MerlinAbilitySlice extends AbilitySlice {
    private static final HiLogLabel LABEL_LOG = new HiLogLabel(3, 0xD001100, "MerlinAbilitySlice");
    protected Merlin merlin;
    private DemoLogHandle logHandle;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        HiLog.info(LABEL_LOG, "MerlinAbility onStart");
        logHandle = new DemoLogHandle();
    }

    @Override
    protected void onActive() {
        super.onActive();
        merlin = createMerlin();
        Logger.attach(logHandle);
        merlin.bind();
    }

    /**
     * createMerlin
     *
     * @return Merlin
     */
    protected abstract Merlin createMerlin();

    /**
     * registerConnectable
     *
     * @param connectable
     */
    protected void registerConnectable(Connectable connectable) {
        merlin.registerConnectable(connectable);
    }

    /**
     * registerDisconnectable
     *
     * @param disconnectable
     */
    protected void registerDisconnectable(Disconnectable disconnectable) {
        merlin.registerDisconnectable(disconnectable);
    }

    /**
     * registerBindable
     *
     * @param bindable
     */
    protected void registerBindable(Bindable bindable) {
        merlin.registerBindable(bindable);
    }

    @Override
    protected void onStop() {
        super.onStop();
        merlin.unbind();
        Logger.detach(logHandle);
    }

    /**
     * DemoLogHandle
     *
     * @since 2021-04-13
     */
    private static class DemoLogHandle implements Logger.LogHandle {
        private static final HiLogLabel TAG = new HiLogLabel(3, 0xD001100, "DemoLogHandle");

        @Override
        public void v(Object... message) {
            HiLog.fatal(TAG, message[0].toString());
        }

        @Override
        public void i(Object... message) {
            HiLog.info(TAG, message[0].toString());
        }

        @Override
        public void d(Object... msg) {
            HiLog.debug(TAG, msg[0].toString());
        }

        @Override
        public void d(Throwable throwable, Object... message) {
            HiLog.debug(TAG, message[0].toString(), throwable);
        }

        @Override
        public void w(Object... message) {
            HiLog.warn(TAG, message[0].toString());
        }

        @Override
        public void w(Throwable throwable, Object... message) {
            HiLog.warn(TAG, message[0].toString(), throwable);
        }

        @Override
        public void e(Object... message) {
            HiLog.error(TAG, message[0].toString());
        }

        @Override
        public void e(Throwable throwable, Object... message) {
            HiLog.error(TAG, message[0].toString(), throwable);
        }
    }
}