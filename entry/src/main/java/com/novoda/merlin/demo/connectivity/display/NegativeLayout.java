package com.novoda.merlin.demo.connectivity.display;

import com.novoda.merlin.demo.ResourceTable;

import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * NegativeLayout
 *
 * @since 2021-04-13
 */
public class NegativeLayout extends ComponentContainer {
    private Text toastText;

    /**
     * NegativeLayout(Context context)
     *
     * @param context
     */
    public NegativeLayout(Context context) {
        this(context,null);
    }

    /**
     * NegativeLayout(Context context, AttrSet attrSet)
     *
     * @param context
     * @param attrSet
     */
    public NegativeLayout(Context context, AttrSet attrSet) {
        this(context, attrSet,"");
    }

    /**
     * NegativeLayout(Context context, AttrSet attrSet, String styleName)
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public NegativeLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView(context);
    }

    private void initView(Context context) {
        DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_negtive_toast, null, false);
        toastText = (Text)toastLayout.findComponentById(ResourceTable.Id_msg_toast);
        addComponent(toastLayout);
    }

    /**
     * setText
     *
     * @param message
     */
    public void setText(String message) {
        toastText.setText(message);
    }
}