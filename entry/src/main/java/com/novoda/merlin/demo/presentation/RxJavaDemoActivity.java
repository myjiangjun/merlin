package com.novoda.merlin.demo.presentation;

import com.novoda.merlin.demo.presentation.slice.RxJavaDemoActivitySlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * RxJavaDemoActivity
 *
 * @since 2021-04-13
 */
public class RxJavaDemoActivity extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(RxJavaDemoActivitySlice.class.getName());
    }
}
