package com.novoda.merlin.demo.presentation.slice;

import com.novoda.merlin.Endpoint;
import com.novoda.merlin.Merlin;
import com.novoda.merlin.MerlinFlowable;
import com.novoda.merlin.MerlinsBeard;
import com.novoda.merlin.NetworkStatus;
import com.novoda.merlin.demo.ResourceTable;
import com.novoda.merlin.demo.connectivity.display.NetworkStatusDisplayer;
import com.novoda.merlin.demo.view.MaterialRippleLayout;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

/**
 * RxJava2DemoActivitySlice
 *
 * @since 2021-04-13
 */
public class RxJava2DemoActivitySlice extends AbilitySlice {
    private static final int TITLE_COLOR = Color.getIntColor("#808080");
    private static final float RIPPLE_ALPHA = 0.2f;
    private static final int RIPPLE_DURATION = 2000;
    private static final int RIPPLE_DIAMETERDP = 35;
    private NetworkStatusDisplayer networkStatusDisplayer;
    private MerlinsBeard merlinsBeard;
    private CompositeDisposable disposables;
    private MaterialRippleLayout materialRippleLayout;
    private Endpoint endpoint;

    private final MaterialRippleLayout.OnClickButton networkStatusOnClick = new MaterialRippleLayout.OnClickButton() {
        @Override
        public void onClick() {
            if (networkStatusDisplayer != null) {
                if (merlinsBeard.isConnected()) {
                    networkStatusDisplayer.displayPositiveMessage(
                            ResourceTable.String_current_status_network_connected);
                } else {
                    networkStatusDisplayer.displayNegativeMessage(
                            ResourceTable.String_current_status_network_disconnected);
                }
            }
        }
    };

    private final MaterialRippleLayout.OnClickButton hasInternetAccessClick = new MaterialRippleLayout.OnClickButton() {
        @Override
        public void onClick() {
            merlinsBeard.hasInternetAccess(hasAccess -> {
                if (networkStatusDisplayer != null) {
                    if (hasAccess) {
                        networkStatusDisplayer.displayPositiveMessage(ResourceTable.String_has_internet_access_true);
                    } else {
                        networkStatusDisplayer.displayNegativeMessage(ResourceTable.String_has_internet_access_false);
                    }
                }
            });
        }
    };

    private final MaterialRippleLayout.OnClickButton wifiConnectedOnClick = new MaterialRippleLayout.OnClickButton() {
        @Override
        public void onClick() {
            if (networkStatusDisplayer != null) {
                if (merlinsBeard.isConnectedToWifi()) {
                    networkStatusDisplayer.displayPositiveMessage(ResourceTable.String_wifi_connected);
                } else {
                    networkStatusDisplayer.displayNegativeMessage(ResourceTable.String_wifi_disconnected);
                }
            }
        }
    };

    private final MaterialRippleLayout.OnClickButton mobileConnectedOnClick = new MaterialRippleLayout.OnClickButton() {
        @Override
        public void onClick() {
            if (networkStatusDisplayer != null) {
                if (merlinsBeard.isConnectedToMobileNetwork()) {
                    networkStatusDisplayer.displayPositiveMessage(ResourceTable.String_mobile_connected);
                } else {
                    networkStatusDisplayer.displayNegativeMessage(ResourceTable.String_mobile_disconnected);
                }
            }
        }
    };

    private final MaterialRippleLayout.OnClickButton networkSubtypeOnClick = new MaterialRippleLayout.OnClickButton() {
        @Override
        public void onClick() {
            if (networkStatusDisplayer != null) {
                networkStatusDisplayer.displayNetworkSubtype();
            }
        }
    };

    private final MaterialRippleLayout.OnClickButton nextActivityOnClick = new MaterialRippleLayout.OnClickButton() {
        @Override
        public void onClick() {
            Intent mIntent = new Intent();
            Operation mOperation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName("com.novoda.merlin.demo")
                    .withAbilityName("com.novoda.merlin.demo.presentation.RxJava2DemoActivity")
                    .build();
            mIntent.setOperation(mOperation);
            startAbility(mIntent);
        }
    };

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(TITLE_COLOR);
        super.setUIContent(ResourceTable.Layout_ability_demo);
        Text title = (Text) findComponentById(ResourceTable.Id_tx_title);
        title.setText(ResourceTable.String_rxjava2_demo_activity);
        disposables = new CompositeDisposable();

        Button csBtn = (Button) findComponentById(ResourceTable.Id_current_status);
        startAnimator(csBtn);
        materialRippleLayout.setOnClick(networkStatusOnClick);
        Button hiaBtn = (Button) findComponentById(ResourceTable.Id_has_internet_access);
        startAnimator(hiaBtn);
        materialRippleLayout.setOnClick(hasInternetAccessClick);
        Button wcBtn = (Button) findComponentById(ResourceTable.Id_wifi_connected);
        startAnimator(wcBtn);
        materialRippleLayout.setOnClick(wifiConnectedOnClick);
        Button mcBtn = (Button) findComponentById(ResourceTable.Id_mobile_connected);
        startAnimator(mcBtn);
        materialRippleLayout.setOnClick(mobileConnectedOnClick);
        Button nsBtn = (Button) findComponentById(ResourceTable.Id_network_subtype);
        startAnimator(nsBtn);
        materialRippleLayout.setOnClick(networkSubtypeOnClick);
        Button naBtn = (Button) findComponentById(ResourceTable.Id_next_activity);
        startAnimator(naBtn);
        materialRippleLayout.setOnClick(nextActivityOnClick);
    }

    /**
     * 启动动画
     *
     * @param bt
     */
    private void startAnimator(Button bt) {
        if (bt != null) {
            materialRippleLayout = new MaterialRippleLayout(this).on(bt)
                    .rippleColor(Color.getIntColor("#86666666"))
                    .rippleAlpha(RIPPLE_ALPHA)
                    .rippleDuration(RIPPLE_DURATION)
                    .rippleBackground(Color.getIntColor("#D1D1D1"))
                    .rippleOverlay(true)
                    .rippleDiameterDp(RIPPLE_DIAMETERDP)
                    .create();
        }
    }

    @Override
    public void onActive() {
        super.onActive();
        endpoint = Endpoint.from("https://www.baidu.com");
        merlinsBeard = new MerlinsBeard.Builder()
                .withEndpoint(endpoint)
                .build(this);
        networkStatusDisplayer = new NetworkStatusDisplayer(getContext(), merlinsBeard);
        Disposable merlinDisposable = MerlinFlowable.from(this,new Merlin.Builder().withEndpoint(endpoint))
                .distinctUntilChanged()
                .subscribe(new NetworkConsumer(networkStatusDisplayer));
        disposables.add(merlinDisposable);
    }

    @Override
    protected void onInactive() {
        super.onInactive();
        networkStatusDisplayer.reset();
        disposables.clear();
    }

    @Override
    protected void onStop() {
        super.onStop();
        networkStatusDisplayer = null;
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    /**
     * NetworkConsumer
     *
     * @since 2021-04-13
     */
    private static class NetworkConsumer implements Consumer<NetworkStatus> {
        private final NetworkStatusDisplayer networkStatusDisplayer;

        NetworkConsumer(NetworkStatusDisplayer networkStatusDisplayer) {
            this.networkStatusDisplayer = networkStatusDisplayer;
        }

        @Override
        public void accept(@NonNull NetworkStatus networkStatus) {
            if (networkStatus.isAvailable()) {
                networkStatusDisplayer.displayPositiveMessage(ResourceTable.String_connected);
            } else {
                networkStatusDisplayer.displayNegativeMessage(ResourceTable.String_disconnected);
            }
        }
    }
}
