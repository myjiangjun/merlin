package com.novoda.merlin.demo;

import ohos.aafwk.ability.AbilityPackage;

/**
 * MyApplication
 *
 * @since 2021-04-13
 */
public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
    }
}
