package com.novoda.merlin.demo.presentation;

import com.novoda.merlin.demo.presentation.slice.HomeAbilitySlice;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

/**
 * HomeAbility
 *
 * @since 2021-04-13
 */
public class HomeAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(HomeAbilitySlice.class.getName());
    }
}
