package com.novoda.merlin.demo.connectivity.display;

import com.novoda.merlin.demo.ResourceTable;

import ohos.agp.components.AttrSet;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * PositiveLayout
 *
 * @since 2021-04-13
 */
public class PositiveLayout extends ComponentContainer {
    private Text toastText;

    /**
     * PositiveLayout(Context context)
     *
     * @param context
     */
    public PositiveLayout(Context context) {
        this(context,null);
    }

    /**
     * PositiveLayout(Context context, AttrSet attrSet)
     *
     * @param context
     * @param attrSet
     */
    public PositiveLayout(Context context, AttrSet attrSet) {
        this(context, attrSet,"");
    }

    /**
     * PositiveLayout(Context context, AttrSet attrSet, String styleName)
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public PositiveLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView(context);
    }

    private void initView(Context context) {
        DirectionalLayout toastLayout = (DirectionalLayout) LayoutScatter.getInstance(context)
                .parse(ResourceTable.Layout_positive_toast, null, false);
        toastText = (Text)toastLayout.findComponentById(ResourceTable.Id_msg_toast);
        addComponent(toastLayout);
    }

    /**
     * setText
     *
     * @param message
     */
    public void setText(String message) {
        toastText.setText(message);
    }
}